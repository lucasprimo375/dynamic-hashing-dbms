import java.io.*;
import java.util.*;
public class Main {
	public static void main(String[] args) {
		try{
			Scanner keyboard = new Scanner(System.in);
			
			while( true ){
				Hash hash = new Hash(4,2);
				int escolha;
				System.out.println( "Escolha sua próxima ação:" );
				System.out.println( "0 - Buscar" );
				System.out.println( "1 - Adicionar" );
				System.out.println( "2 - Remover" );
				System.out.println( "3 - Sair" );
				System.out.print( "Sua escolha:" );
				escolha = keyboard.nextInt();
				
				if( escolha == 3 ){
					break;
				}
				
				switch( escolha ){
					case 0:
						System.out.print( "Entre a chave a ser buscada:" );
						int chave = keyboard.nextInt();
						String s = "";
						s = hash.buscar( chave );
						System.out.println( s );
						break;
						
					case 1:
						System.out.print( "Entre a chave do registro: " );
						chave = keyboard.nextInt();
						System.out.print( "Entre o valor do registro: " );
						String valor = keyboard.next();
						hash.adicionar( chave, valor );
						break;
						
					case 2:
						System.out.print( "Entre a chave do registro: " );
						chave = keyboard.nextInt();
						System.out.print( "Entre o valor do registro: " );
						valor = keyboard.next();
						hash.remover( chave, valor );
						break;
						
					default:
						System.out.println( "Escolha um número entre 0, 1, 2, 3." );
						break;
				}
			}
			/*Hash hash = new Hash(4,2);
			Scanner ler = new Scanner(System.in);
			FileReader arq = new FileReader("Banco.txt");
      		BufferedReader lerArq = new BufferedReader(arq);
      		
      		hash.buscar( 0 );*/
       		/*String linha = lerArq.readLine();
       		String a[] = new String[4];
       		int j =0;
       		for(int i=0; i<4; i++){	
       			a[i]="";
       		}
       		for(int i=0; i<linha.length(); i++){
       			if(linha.charAt(i)!='#'){
       				a[j]+=linha.charAt(i);
       			} else {
       				j++;
       			}
       		}
			
			// a[0]: quantos cabem
			// a[1]: quantos tem
			int qtd = Integer.parseInt( a[1] );
			
			System.out.println( "Mostrando os registros no arquivo" );
			for( int i=2; i<qtd+2; i++ ){
				System.out.println( a[i] );
			}*/
		}
		catch(IOException ioe){
		}
		finally{}
	}
}
