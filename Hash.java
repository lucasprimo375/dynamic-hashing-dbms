import java.io.*;
import java.util.*;

public class Hash{
	private int rodada, m, n, p, atual, novo;
	
	public Hash(int m, int n){
		this.m = m;
		this.n = n;
		rodada = 0;
		p = 0;
		atual = m;
		novo = 2*m;
		
	}
	
	public void remover( int chave, String valor ) throws IOException {
		int bucket = chave % atual;
		if( bucket < p ){
			bucket = chave % novo; 
		}
		
		int count = 0; 
		BufferedReader buffRead = new BufferedReader(new FileReader("Banco.txt"));
   		String linha = buffRead.readLine();
    	while (( linha != null ) && ( count < bucket )) {
        	linha = buffRead.readLine();
        	count++;
    	}
    	
    	int tam = linha.length();
    	int qtdAtual = 0;
    	for( int i=0; i<tam; i++ ){
    		if( linha.charAt(i) == ',' ){
    			qtdAtual++;
    		}
    	}
    	
    	if( qtdAtual > 0 ){
			String linha1 = buscar( chave );
			tam = linha1.length();
			
			String registros[] = new String[qtdAtual];
			for( int i=0; i<qtdAtual; i++ ){
				registros[i] = "";		
			}
			
			int j=0;
			for( int i=0; i<tam; i++ ){
				if( linha1.charAt(i) != '#' ){
					registros[j] += linha1.charAt(i);
				} else {
					j++;
				}
			}
			
			boolean var[] = new boolean[qtdAtual];
			for( int i=0; i<qtdAtual; i++ ){
				int rTam = registros[i].length();
				int v=0;
				for( j=0; j<rTam; j++ ){
					if( registros[i].charAt(j) == ',' ){
						v = j;
					}
				}
				String value = "";
				for( j=v+1; j<rTam; j++ ){
					value += registros[i].charAt( j );
				}
				//System.out.println( value );
				
				if( value.equals( valor ) ){
					var[i] = true;
				} else {
					//System.out.println( registros[i] );
					var[i] = false;
				}
			}
			
			String novaLinha = "2#" + String.valueOf(qtdAtual-1) + "#";
			for( int i=0; i<qtdAtual; i++ ){
				if( !var[i] ){
					//System.out.println( registros[i] );
					novaLinha += registros[i] + "#";
				}
			}
			
			//System.out.println( novaLinha );
			
			alteraLinha( null, novaLinha, bucket );
		}
	}
	
	public void adicionar( int chave, String valor ) throws IOException {
		int bucket = chave % atual;
		if( bucket < p ){
			bucket = chave % novo; 
		}
		
		int count = 0; 
		BufferedReader buffRead = new BufferedReader(new FileReader("Banco.txt"));
   		String linha = buffRead.readLine();
    	while (( linha != null ) && ( count < bucket )) {
        	linha = buffRead.readLine();
        	count++;
    	}
    	
    	// pegando o campo da quantidade maxima de registros em um bucket
    	int i = 0;
    	while( linha.charAt(i) != '#' ){
    		i++;
    	}
    	
    	// pegando o campo da quantidade atual de registros em um bucket
    	int qtdMax = Integer.parseInt( linha.substring(0,i) );
    	int t = i;
    	i = 0;
    	int j = 0;
    	while( j<2 ){
    		if( linha.charAt(i) == '#' ){
    			j++;
    		}
    		
    		i++;
    	}
    	
    	int qtdAtual = Integer.parseInt( linha.substring(t+1,i-1) );
    	
    	// pegando a posição a partir da qual o registros aparecem
		i = 0;
		j = 0;
		while( j<2 ){
			if( linha.charAt(i) == '#' ){
				j++;
			}
			
			i++;
		}
		
		String linhaAntiga = buscar( chave );
		linha = String.valueOf(qtdMax) + "#" + String.valueOf(qtdAtual+1) + linha.substring( i-1, linha.length()) + String.valueOf(chave) + "," + valor + "#";
		alteraLinha( linhaAntiga, linha, bucket );
		
		if( qtdAtual+1 > n ){
			qtdAtual++;
			linha = buscar( chave );
			int linhaTam = linha.length();
			String registros[] = new String[qtdAtual];
			for( i=0; i<qtdAtual; i++ ){
				registros[i] = "";
			}
			
			// separando a linha em registros
			j=0;
			for( i=0; i<linhaTam; i++ ){
				if( j == qtdAtual ){
					break;
				}
				if( linha.charAt(i) != '#' ){
					registros[j] += linha.charAt(i);
				} else {
					j++;
				}
			}
			
			// checando quais registros continuam na linha e quais vão para a linha nova
			int contAtual = 0;
			int contNovo = 0;
			boolean ver[] = new boolean[qtdAtual];
			for( i=0; i<qtdAtual; i++ ){
				int tam = registros[i].length();
				String chaveAtual = "";
				for( j=0; j<tam; j++ ){
					if( registros[i].charAt(j) != ',' ){
						chaveAtual += registros[i].charAt(j);
					} else {
						break;
					}
				}
				int chaveAtualInt = Integer.parseInt( chaveAtual );
				
				if( chaveAtualInt % novo == p ){
					contAtual++;
					ver[i] = true;
				} else {
					contNovo++;
					ver[i] = false;
				}
			}
			
			// construindo as linhas atual e a nova
			linha = String.valueOf(n) + "#" + String.valueOf(contAtual) + "#";
			String linhaNova = String.valueOf(n) + "#" + String.valueOf(contNovo) + "#";
			for( i=0; i<qtdAtual; i++ ){
				if( ver[i] ){
					linha += registros[i] + "#";
				} else {
					linhaNova += registros[i] + "#";
				}
			}
			
			String arquivo = "Banco.txt";
			String arquivoTmp = "ARQUIVO-tmp";

			BufferedWriter writer = new BufferedWriter(new FileWriter(arquivoTmp));
			BufferedReader reader = new BufferedReader(new FileReader(arquivo));

			String linha1;
			i=0;
			while ((linha1 = reader.readLine()) != null) {
				if( i == bucket ){
					linha1 = linha;
				}
				writer.write(linha1 + "\n");
				i++;
			}
			writer.write(linhaNova + "\n");

			writer.close();        
			reader.close();

			new File(arquivo).delete();
			new File(arquivoTmp).renameTo(new File(arquivo));
			
			p++;
			
			if( p == (int)Math.pow(2, rodada)*m ){
				p = 0;
				atual = novo;
				rodada++;
				novo = (int)Math.pow(2, rodada)*m;
			}
		}
	}
	
	public void alteraLinha(String palavraAntiga, String palavraNova, int bucket) throws IOException {
		String arquivo = "Banco.txt";
		String arquivoTmp = "ARQUIVO-tmp";

		BufferedWriter writer = new BufferedWriter(new FileWriter(arquivoTmp));
		BufferedReader reader = new BufferedReader(new FileReader(arquivo));

		String linha;
		int cont = 0;
		while ((linha = reader.readLine()) != null) {
		    if ( cont == bucket ) {
		        linha = palavraNova;
		    }
		    writer.write(linha + "\n");
		    cont++;
		}

		writer.close();        
		reader.close();

		new File(arquivo).delete();
		new File(arquivoTmp).renameTo(new File(arquivo));
	}
 	
	public String buscar( int chave ) throws IOException{
		int bucket = chave % atual;
		if( bucket < p ){
			bucket = chave % novo; 
		}
		
		int count = 0; 
		BufferedReader buffRead = new BufferedReader(new FileReader("Banco.txt"));
   		String linha = buffRead.readLine();
    	while (( linha != null ) && ( count < bucket )) {
    		//System.out.println( linha );
        	linha = buffRead.readLine();
        	count++;
    	}
    	
    	if( linha == null ){
    		return null;
    	}
    	
    	// retirando a parte que indica a quantidade máxima e a quantidade atual
    	int i = 0;
    	int j = 0;
    	while( i<2 ){
    		if( linha.charAt(j) == '#' ){
    			linha = linha.substring( j );
    			i++;
    		}
    		
    		j++;
    	}
    	
    	// retirando o # da frente
    	linha = linha.substring( 1, linha.length() );
    	
    	return linha;
	}
}
